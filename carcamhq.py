import subprocess
import gpsd
from RPLCD.gpio import CharLCD
import time
from gpiozero import Button  
from RPi import GPIO
import zmq
import carcamlib
import socket
import threading
from buttonpress import capture_button_press

def main():
    context = zmq.Context.instance()

    subscriber = context.socket(zmq.SUB)
    subscriber.bind("tcp://192.168.4.1:5556")
    subscriber.setsockopt(zmq.SUBSCRIBE, b"False")   

    publisher = context.socket(zmq.PUB)
    publisher.bind("tcp://192.168.4.1:5557") #"tcp://*:5557" was in several posts
    button_receiver = context.socket(zmq.PAIR)
    button_receiver.bind("inproc://button_capture")
    poller = zmq.Poller()
    poller.register(subscriber, zmq.POLLIN)
    poller.register(button_receiver, zmq.POLLIN)

#    button_red = Button(26)
#    button_lblue = Button(15)
#    button_rblue = Button(13)
#    button_white = Button(6)

    lcd = CharLCD(cols=16, rows=2, pin_rs=24, pin_e=23, pins_data=[17, 18, 27, 22], numbering_mode=GPIO.BCM)
    hostname = socket.gethostname()
    carcamhq_status = carcamlib.StatusUpdate(carcam_id=hostname, command_role='True')
    button_thread = threading.Thread(target=capture_button_press, args=('Buttons',))
    button_thread.start()
    gpsd.connect()

    lcd.clear()
    lcd.write_string('CarCam HQ')
    time.sleep(2)
    lcd.clear()

    left_update_message = ''
    right_update_message = ''
    front_update_message = ''
    back_update_message = ''
    message_list = [left_update_message,
            right_update_message,
            front_update_message,
            back_update_message]

    lcd.write_string('Acquiring GPS Signal Mode > 1')
    mode = 0 
    counter = 0
    rounds = 0
    carcamhq_status.ready_status = False    
    while True: 
        rounds += 1       
        try:
            mode = gpsd.get_current().mode
            print('Mode: {}'.format(mode))
            if mode > 1:
                lcd.clear()
                lcd.write_string('Signal Acquired')
                time.sleep(2)
                lcd.clear()
                carcamhq_status.ready_status = True
                break
            elif rounds > 100:
                lcd.clear()
                lcd.write_string('Failed to Achieve > 1 GPS Mode')
                time.sleep(2)
                lcd.clear()
                carcamhq_status.ready_status = False
                break
            else:
                lcd.clear()
                lcd.write_string('Round {} of Signal Acquisition'.format(rounds))
                time.sleep(1)
                lcd.clear()
        except:
            continue
        time.sleep(1)

    while True:
        print('Begin Update Loop')
        for recv_iter in range(1,11):       
            try:
                print('Begin poll')
                #poller.poll() returns a list of tuples (socket, event) that are ready 
                status_updates = dict(poller.poll(.1))
                print(status_updates)
                len(status_updates)
                print('Polling Complete')
            except KeyboardInterrupt:
                break 
            if subscriber in status_updates:
                counter = 0
                print('Received an update')
                update = carcamlib.receive_status(subscriber)
                print(update.carcam_id)
                print(update.ready_status)
                print(update.imaging_status)
                print(update.image_number)
                print(update.command_role)
                print(update.shutdown_status)
                    
                if (carcamhq_status.ready_status == True 
                        and carcamhq_status.imaging_status == False): 
                    message_list = carcamlib.parse_status(update, 
                            'ready_status', 
                            left_update_message,
                            right_update_message,
                            front_update_message,
                            back_update_message)    
                
                elif (carcamhq_status.ready_status == True 
                        and carcamhq_status.imaging_status == True): 
                    message_list = carcamlib.parse_status(update, 
                            'image_number', 
                            left_update_message,
                            right_update_message,
                            front_update_message,
                            back_update_message)    
                
                elif (carcamhq_status.ready_status == True 
                        and carcamhq_status.imaging_status == False):
                    message_list = carcamlib.parse_status(update, 
                            'imaging_status', 
                            left_update_message,
                            right_update_message,
                        front_update_message,
                        back_update_message)    

            else:
                counter += 1
                if counter > 100:
                    lcd.clear()
                    lcd.write_string('No Imagers Detected')
        #            time.sleep(2)
        #            lcd.clear()

            if button_receiver in status_updates:
                button_color = button_receiver.recv()
                print(button_color)
                carcamhq_status = carcamlib.parse_button(lcd, button_color, 
                         carcamhq_status)
                carcamlib.send_status(carcamhq_status, publisher)
            if carcamhq_status.shutdown_status == True:
                button_receiver.send(b"stop")
                button_thread.join()
                time.sleep(30)
                subprocess.call(['sudo', 'shutdown','now'])
    #        time.sleep(1)
        carcamlib.write_status(lcd, *message_list)
        carcamlib.send_status(carcamhq_status, publisher)
        print('HQ Ready Status: {}'.format(carcamhq_status.ready_status))
        print('HQ Imaging Status: {}'.format(carcamhq_status.imaging_status))

        time.sleep(1)
if __name__ == '__main__':
    main()
