import math

def dd_to_dms(dd_lat, dd_lon):
    

    
    dms_deg_lat_double = abs(dd_lat)
    dms_deg_lat_int = int(dms_deg_lat_double)
#    print(str(dms_deg_lat_double))
#    print(str(dms_deg_lat_int))
    dms_m_lat_double = (dms_deg_lat_double - dms_deg_lat_int)*60
    dms_m_lat_int = int(dms_m_lat_double)
#    print(str(dms_m_lat_double))
#    print(str(dms_m_lat_int))

    dms_s_lat_double = (dms_m_lat_double - dms_m_lat_int)*60
    #Multiplying by 100 since exif standard has seconds devided by 100
    dms_s_lat_double = dms_s_lat_double *100
    dms_s_lat_int = int(dms_s_lat_double)
#    print(str(dms_s_lat_int))

#    deg_lat = dms_deg_lat_int.to_bytes(4, 'little')
#    m_lat = dms_m_lat_int.to_bytes(4, 'little')
#    s_lat = dms_s_lat_int.to_bytes(4, 'little')
#    one = 1
#    hundred = 100
#    one = one.to_bytes(4, 'little')
#    hundred = hundred.to_bytes(4, 'little')
#    latitude = deg_lat + one + m_lat + one + s_lat + hundred 
    dms_lat = str(dms_deg_lat_int) + '/1,' + str(dms_m_lat_int) + '/1,' + str(dms_s_lat_int) + '/100'
   
    
    dms_deg_lon_double = abs(dd_lon)
    dms_deg_lon_int = int(dms_deg_lon_double)
#    print(str(dms_deg_lon_double))
#    print(str(dms_deg_lon_int))
    dms_m_lon_double = (dms_deg_lon_double - dms_deg_lon_int)*60
    dms_m_lon_int = int(dms_m_lon_double)
#    print(str(dms_m_lon_double))
#    print(str(dms_m_lon_int))
    
    dms_s_lon_double = (dms_m_lon_double - dms_m_lon_int)*60
    dms_s_lon_double = dms_s_lon_double * 100
    dms_s_lon_int = int(dms_s_lon_double)
#    print(str(dms_s_lon_int))
    
#    deg_lon = dms_deg_lon_int.to_bytes(4, 'little')
#    m_lon = dms_m_lon_int.to_bytes(4, 'little')
#    s_lon = dms_s_lon_int.to_bytes(4, 'little')
#    one = 1
#    hundred = 100
#    one = one.to_bytes(4, 'little')
#    hundred = hundred.to_bytes(4, 'little')
#    longitude = deg_lon + one + m_lon + one + s_lon + hundred 
   
    
    dms_lon = str(dms_deg_lon_int) + '/1,' + str(dms_m_lon_int) + '/1,' + str(dms_s_lon_int) + '/100'
    
    if dd_lat > 0:
        ns = 'N'
    else:
        ns = 'S'
    if dd_lon > 0:
        ew = 'E'
    else:
        ew = 'W'
    return (dms_lat, ns, dms_lon, ew)

def time_stamp_convert(gps_time_stamp):
    date_time_list = gps_time_stamp.split('T')
    time_list = date_time_list[1].split(':')
    time_out = time_list[0] + '/1,' + time_list[1] + '/1,' + time_list[2][:2] + '/1'
    date_out = date_time_list[0].replace('-', ':')
    return (date_out, time_out)
    

