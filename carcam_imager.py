#!/usr/bin/python3
from time import sleep
import socket
#import struct
import threading
import signal
import subprocess
from threaded_image_capture import capture
#import select
import zmq
import carcamlib

def main():
    context = zmq.Context.instance()
    subscriber = context.socket(zmq.SUB)
    subscriber.connect("tcp://192.168.4.1:5557")
    subscriber.setsockopt(zmq.SUBSCRIBE, b"True")
    
    publisher = context.socket(zmq.PUB)
    publisher.connect("tcp://192.168.4.1:5556")
    
    capture_comm = context.socket(zmq.PAIR)
    capture_comm.bind("inproc://image_capture")

    poller = zmq.Poller()
    poller.register(subscriber, zmq.POLLIN)
    poller.register(capture_comm, zmq.POLLIN)
    thread_list = []
    hostname = socket.gethostname()
    imager_status = carcamlib.StatusUpdate(carcam_id = hostname)
#    collecting = False
    #thread_list.append(threading.Thread(target=capture, args=('Carcam_Signal',))) 
    
    imager_status.ready_status = True
    print(imager_status.command_role)
    carcamlib.send_status(imager_status, publisher)
#    sock.sendto(bytes(imager_id + 'Ready,', 'utf-8'), multicast_group)
    while True:
#        carcamlib.send_status(imager_status, publisher)
        for recv_iter in range(1,10):
            try:
                print("Begin Poll")
                status_updates = dict(poller.poll(1))
                print("End Poll")
            except KeyboardInterrupt:
                break
            if subscriber in status_updates:
                update = carcamlib.receive_status(subscriber)
                print("Carcam ID: " + update.carcam_id)
                print("Ready Status: " + str(update.ready_status))
                print("Imaging Status: " + str(update.imaging_status))
                print("Command Role: " + str(update.command_role))
                print("Shutdown Status: " + str(update.shutdown_status))
                print("Image Num: " + str(update.image_number))
                print(threading.enumerate())
                print("Carcam ID: " + imager_status.carcam_id)
                print("Ready Status: " + str(imager_status.ready_status))
                print("Imaging Status: " + str(imager_status.imaging_status))
                print("Command Role: " + str(imager_status.command_role))
                print("Shutdown Status: " + str(imager_status.shutdown_status))
                print("Image Num: " + str(imager_status.image_number))
                if (imager_status.ready_status == True and 
                        update.imaging_status == True):
                    print('received begin signal')
    #                current_image_number = 0 
                    if threading.active_count() == 1 or not thread_list[-1].is_alive():
                        print('Active Thread Count is == 1. Trying to start new thread')
                        thread_list.append(threading.Thread(target=capture, args=('Carcam_Signal',))) 
                        thread_list[-1].start()
                        imager_status.imaging_status = True
                    else:
                        continue
                elif (imager_status.imaging_status == True 
                        and update.imaging_status == False):
                    print('received stop signal')

                    if threading.active_count() == 1:
                        continue
                    elif thread_list[-1].is_alive():
    #                    thread_list[-1].do_run = False
                        capture_comm.send(b'stop')                    
                        thread_list[-1].join()
#                        collecting = False
                        print('Thread Still Alive?: {}'.format(thread_list[-1].is_alive))
                        imager_status.imaging_status = False
                        print('Thread Joined')
                    else:
                        print('Possible Error. Most recent thread not the currently active thread')
                        continue
                else:
                    print('nothing')
                if update.shutdown_status == True:
                    print('received shutdown signal')
                    if threading.active_count() == 1:
                        subprocess.run(['sudo', 'shutdown', 'now'])
                    elif thread_list[-1].is_alive():
                        capture_comm.send(b'stop')
                        thread_list[-1].join()
                        print('Thread Joined')
                        subprocess.run(['sudo', 'shutdown', 'now'])
                    else:
                        print('Possible Error. Most recent thread not the currently active thread')
            if capture_comm in status_updates:
                print("Received an image number")
                image_num_from_thread = capture_comm.recv() 
                image_num_from_thread = int.from_bytes(image_num_from_thread, 'little', signed=False)
                print("Image num from thread: "+ str(image_num_from_thread))
                imager_status.image_number = image_num_from_thread
                print("Imager_status.imagenumber: "+ str(imager_status.image_number))
#            print(type(imager_status.image_number))
#            print(imager_status.image_number)
        carcamlib.send_status(imager_status, publisher)
        sleep(2)
if __name__ == '__main__':
    main()


