import subprocess
from RPLCD.gpio import CharLCD
import time
from gpiozero import Button  
from RPi import GPIO
import zmq
import carcamlib


def capture_button_press(arg):
    print(arg)
    context = zmq.Context.instance()

    button_sender = context.socket(zmq.PAIR)
    button_sender.connect("inproc://button_capture")
    
    poller = zmq.Poller()
    poller.register(button_sender, zmq.POLLIN)

    button_red = Button(26)
    button_lblue = Button(15)
    button_rblue = Button(13)
    button_white = Button(6)

    run = True

    while run:    
        if button_white.is_pressed:
            button_sender.send(b"white")
        elif button_lblue.is_pressed:
            button_sender.send(b"lblue")
        elif button_rblue.is_pressed:
            button_sender.send(b"rblue")
        elif button_red.is_pressed:
            button_sender.send(b"red")
        message = dict(poller.poll(.1))
        if button_sender in message:
            signal = button_sender.recv()
            if signal == b'stop':
                run = False
        time.sleep(.3)
def main():
     capture_button_press("Buttons")


if __name__ == '__main__':
    main()
