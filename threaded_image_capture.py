from picamera import PiCamera
import time
import datetime
import math
from gps3.agps3threaded import AGPS3mechanism
import signal
import os
from pathlib import Path
import time
import threading 
import faulthandler
import sys
import zmq
from gpsconvert import dd_to_dms, time_stamp_convert

    
def capture(arg):
    print(arg)
    
    context = zmq.Context.instance()
    capture_comm = context.socket(zmq.PAIR)
    capture_comm.connect("inproc://image_capture")
    
    poller = zmq.Poller()
    poller.register(capture_comm, zmq.POLLIN)

    p = threading.currentThread()
    IMG_DIRECTION_CORRECT_L = 0
    SEC_BETWEEN_IMAGE = 3
    begin_signal = False
    current_image_number = 0

    camera = PiCamera()
    camera.sensor_mode = 2
    camera.resolution = (3280, 2464)
    camera.rotation = 90
    camera.exposure_mode = 'sports'
    today = datetime.datetime.now() 

    # Is the intent here to always create a new directory, 
    # appending "Boom" as many times as necessary to get 
    # something new?  If so, here's a way to do it:
    new_dir = Path(f"/home/pi/Pictures/{today.strftime('%Y-%m-%d-%H_%M_%S')}")
    while new_dir.exists():
        new_dir += "Boom"
    try:
        new_dir.mkdir(parents=True, exist_ok=True)
    except Exception as exc:
        print(f'Failed to create image directory.  Error: {exc}')

    gpsd_initial_connect = True
    agps_thread = AGPS3mechanism()
    agps_thread.stream_data(host='192.168.4.1', port=2947)
    agps_thread.run_thread()
    while agps_thread.data_stream.mode == 'n/a':
        time.sleep(.1)

    old_gps_data_time = agps_thread.data_stream.time
    do_run = True
    while do_run:   
        while old_gps_data_time == agps_thread.data_stream.time:
            time.sleep(0.01)
            continue
        old_gps_data_time = agps_thread.data_stream.time
        if agps_thread.data_stream.mode >= 2 and agps_thread.data_stream.speed >= 1:
            (hms_lat, ns, hms_lon, ew) = dd_to_dms(agps_thread.data_stream.lat, agps_thread.data_stream.lon)
            (gps_date, gps_time) = time_stamp_convert(agps_thread.data_stream.time)
            camera.exif_tags['GPS.GPSTimeStamp'] = gps_time
            camera.exif_tags['GPS.GPSDateStamp'] = gps_date
            camera.exif_tags['GPS.GPSLongitude'] = hms_lon
            camera.exif_tags['GPS.GPSLatitude'] = hms_lat
            camera.exif_tags['GPS.GPSLongitudeRef'] = ew 
            camera.exif_tags['GPS.GPSLatitudeRef'] = ns
            gps_img_direction = (float(agps_thread.data_stream.track) + IMG_DIRECTION_CORRECT_L) % 360
            gps_img_dir = str(int(round(gps_img_direction, 2)*100)) + '.100' 
            camera.exif_tags['GPS.GPSImgDirection'] = gps_img_dir
            camera.exif_tags['GPS.GPSImgDirectionRef'] = 'T'
        else:
            time.sleep(0.1)
            continue
        current_image_path = new_dir / f"/OSM_L_{current_image_number}.jpeg"
        camera.capture(current_image_path)
        current_image_number += 1
        capture_comm.send((current_image_number).to_bytes(5, 'little'))
        message = dict(poller.poll(.01))
        if capture_comm in message:
            stop_signal = capture_comm.recv()
            if stop_signal == b"stop":
                do_run = False
    camera.close()
    agps_thread.stop()
def main():
    capture('CarCamTest')


if __name__ == '__main__':
    main()

